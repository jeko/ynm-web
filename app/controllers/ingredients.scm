;; Controller ingredients definition of ynm-web
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller ingredients) ; DO NOT REMOVE THIS LINE!!!

(use-modules (ynm-core entities)
	     (ynm-core use-cases)
	     (ynm-core adapters)
	     (json))

(define CSV_CALNUT_BASE "/home/jeko/Workspace/ynm/ynm-web/prv/CALNUT2020_2020_07_07.csv")

(define food->json-input
  (lambda (food)
    (list 
     (cons "alim_code" (number->string (food-code food)))
     (cons "food_label" (food-label food))
     (cons "intakes" 
	   (list
	    (cons "nrj_kj" (energy-kj (food-intake food)))
	    (cons "nrj_kcal" (energy-kcal (food-intake food))))))))

(ingredients-define
 /
 (lambda ()
   (let ([all-ingredients (ingredients (build-calnut-base CSV_CALNUT_BASE))])
     (scm->json-string
      (list
       (cons "count" (number->string (length all-ingredients)))
       (cons "next" #f)
       (cons "previous" #f)
       (cons "results"
	     (list->vector (map food->json-input all-ingredients))))))))
